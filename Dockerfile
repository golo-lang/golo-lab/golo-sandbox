FROM ubuntu:latest
LABEL maintainer="@k33g_org"
 
RUN apt-get update && \
    apt-get install -y apt-utils && \
    apt-get install -y curl && \
    apt-get install -y nano && \
    apt-get install -y gnupg2 && \
    apt install -y openjdk-8-jdk && \
    apt install -y maven && \
    apt install -y mercurial && \
    apt install -y micro && \
    apt install -y git && \
    apt install -y tmux && \
    apt install -y inotify-tools && \
    apt-get -y install locales  && \
    apt-get clean

# Set the locale
RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8  

# Install Golo
RUN cd home && \
    git clone https://github.com/eclipse/golo-lang.git && \
    cd golo-lang && \
    ./gradlew installDist && \
    echo "export GOLO_HOME=/home/golo-lang/build/install/golo" >> ~/.bashrc  && \
    echo "export PATH=\$PATH:\$GOLO_HOME/bin" >> ~/.bashrc

# Set Micro editor colorscheme
RUN mkdir -p ~/.config/micro && \
    echo '{"colorscheme":"simple","tabsize":2}' > ~/.config/micro/settings.json

# Tmux
RUN echo "set -g mouse on" > ~/.tmux.conf && \
    echo "#!/bin/bash" >> home/golo-session.sh && \
    echo 'tmux new-session -s "golo-session" -d' >> home/golo-session.sh && \
    echo "tmux split-window -h" >> home/golo-session.sh && \
    echo "tmux -2 attach-session -d" >> home/golo-session.sh && \
    chmod +x home/golo-session.sh

COPY . /home

WORKDIR /home

