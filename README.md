# Docker Golo Sandbox

**Golo Sandbox** is a docker image for learning [Golo *(a lightweight dynamic language for the JVM)*](https://golo-lang.org/). The latest version of Golo is installed along with a source code editor.

## How to use it

### Pull the Docker image

```bash
docker pull registry.gitlab.com/golo-lang/golo-sandbox:latest
```

### Create a container

```bash
docker run --name golo-try-sandbox -i -t registry.gitlab.com/golo-lang/golo-sandbox:latest
```

You should get a prompt like that:

```bash
root@64919bdb2406:/home#
```

### Start playing with Golo

Type this command:

```bash
./golo-session.sh
```

The script will open a tmux session with 2 panels:

![golo-session-01.png](golo-session-01.png)

- You can edit the code with the provided editor [Micro Editor](https://micro-editor.github.io/). Try `micro hello.golo`
- You can run the code from the other pane. Try `golo golo --files hello.golo`

![golo-session-02.png](golo-session-02.png)


> Micro Editor is easy to use:
> - save: Ctrl + s
> - quit: Ctrl + q

